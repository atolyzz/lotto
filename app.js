const ballsRef = document.querySelectorAll(".ball");
const playRef = document.querySelector("#play");
const resultsRef = document.querySelector("#results");
// const lastResultsRef = document.querySelector("#lastResults");
const newResultsRef = document.querySelector("#newResults");
const url = "http://localhost:3000/hits/";

function randomizeUserInput() {
  const randomNumbers = shuffleDigits();
  randomNumbers.forEach((el, index) => {
    ballsRef[index].value = el;
  });
}

function playGame() {
  randomizeUserInput();
  playRef.click();
}

playRef.addEventListener("click", function(event) {
  event.preventDefault();
  const balls = [...ballsRef];
  balls.forEach(ball => removeValidation(ball));
  const emptyBalls = balls.filter(ball => checkIsEmpty(ball));
  emptyBalls.forEach(ball => showValidation(ball));
  if (emptyBalls.length === 0) {
    console.log("nie ma pustych");
    const notNumberBalls = balls.filter(ball => checkIfNumber(ball));
    notNumberBalls.forEach(ball => showValidation(ball));
    if (notNumberBalls.length === 0) {
      console.log("wszystkie są poprawnymi liczbami");
      const notInScopeBalls = balls.filter(ball => checkIsNotInScope(ball));
      notInScopeBalls.forEach(ball => showValidation(ball));
      if (notInScopeBalls.length === 0) {
        console.log("Wszystkie piłki są w zakresie od 1 do 49.");
        const redundandArray = checkIfRedundant(balls);
        redundandArray.forEach(ball => showValidation(ball));
        if (redundandArray.length === 0) {
          console.log("Żadne numery się nie powtarzają");
          const winningCombo = shuffleDigits();
          const hits = checkHits(balls, winningCombo);
          showResults(hits);
          addToLocalStorage(hits);
        }
      }
    }
  }
});

function checkIsEmpty(input) {
  if (input.value === "") {
    return true;
  } else {
    return false;
  }
}

function checkIfNumber(input) {
  if (
    isNaN(parseInt(input.value)) ||
    parseInt(input.value).toString().length !== input.value.length
  ) {
    return true;
  } else {
    return false;
  }
}

function checkIsNotInScope(input) {
  if (input.value < 1 || input.value > 49) {
    return true;
  } else {
    return false;
  }
}

function checkIfRedundant(balls) {
  const ballsValue = balls.map(ball => ball.value);
  const redundand = balls.filter(
    (ball, index) => ballsValue.indexOf(ball.value, index + 1) !== -1
  );
  return redundand;
}

function shuffleDigits() {
  const digits = [];
  for (let i = 0; i < 6; i++) {
    const digit = Math.round(Math.random() * 48 + 1);
    if (!digits.includes(digit)) {
      digits.push(digit);
    } else {
      i--;
    }
  }
  return digits;
}

function checkHits(userDigits, winningCombo) {
  const userValues = userDigits.map(digit => parseInt(digit.value));
  const hits = winningCombo.filter(hit => userValues.includes(hit));
  return hits;
}

function showResults(results) {
  if (results.length === 0) {
    const messageZero = "Niestety, nic nie trafiłeś :(";
    resultsRef.innerHTML = messageZero;
  } else if (results.length > 0 || results.length < 6) {
    const message1to5 = `Trafiłeś liczb: ${
      results.length
    }!, a trafione liczby to: ${results}`;
    resultsRef.innerHTML = message1to5;
  } else {
    resultsRef.innerHTML = "TRAFIŁEŚ SZÓSTKĘ!!!";
  }
}

function showGlobalResults() {
  const results = localStorage.getItem("klucz");
  if (results) {
    const resultsTab = JSON.parse(results);
    const stats1 = resultsTab
      .filter(el => el.length !== 0)
      .map(el => el.length)
      .reduce((val, acc) => val + acc);
    const stats2 = resultsTab
      .filter(el => el.length !== 0)
      .flat()
      .sort((a, b) => a - b);
    console.log(new Set(stats2));
    const message = `Grałeś ${
      resultsTab.length
    } razy i trafiłeś liczb ${stats1}`;
    newResultsRef.innerText = message;
  }
}

function showValidation(input) {
  input.classList.add("error");
}

function removeValidation(input) {
  input.classList.remove("error");
}

function addToLocalStorage(set1) {
  const key = localStorage.getItem("klucz");
  if (key) {
    const tabKey = JSON.parse(key);
    tabKey.push(set1);
    localStorage.setItem("klucz", JSON.stringify(tabKey));
  } else {
    localStorage.setItem("klucz", JSON.stringify([set1]));
  }
}

// function updateLocalStorage(key, newNumbers) {
//   if(key){
//     const tab = localStorage.getItem(JSON.parse(key));
//     tab.push(newNumbers);
//     localStorage.setItem(key, JSON.stringify(tab));
//   } else{
//      localStorage.setItem(key, JSON.stringify(newNumbers));
//   }
// }

// function getFromLocalStorage(set1, set2) {
//   const set1Value = localStorage.getItem(set1);
//   const set2Value = localStorage.getItem(set2);
//   lastResultsRef.innerText = `Ostatnie wygrane liczby to: ${set1Value}, ostatnie wybrane liczby, to: ${set2Value}`;
// }

function addToDB(link) {
  const allData = localStorage.getItem("klucz");
  fetch(link, {
    method: "POST",
    body: allData,
    headers: { "Content-Type": "application/json" }
  }).then(response => response.json());
}

function multiplePlays(amount) {
  for (let i = 0; i < amount; i++) {
    playGame();
  }
}

multiplePlays(100);

addToDB(url);

showGlobalResults();
